# Fortran - Modelo de Ising - Versão Cairo

Simulação do modelo de Ising para uma malha de partículas com spins +/- 1/2, usando o
algoritmo de Metrópole e a biblioteca gráfica Cairo do GTK.
